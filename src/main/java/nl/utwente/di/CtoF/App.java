package nl.utwente.di.CtoF;


import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class App extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Conversion convert;

    public void init() throws ServletException {
        convert = new Conversion();
    }
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius_Fahrenheit_convertor";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius temperature: " +
                request.getParameter("temp") + "\n" +
                "  <P>Conversion to Fahrenheit: " +
                Integer.toString(convert.getFahrenheit(request.getParameter("temp"))) +
                "</BODY></HTML>");
    }
}