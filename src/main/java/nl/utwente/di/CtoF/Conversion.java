package nl.utwente.di.CtoF;

public class Conversion {
    public static int getFahrenheit(String temp){
        int a = Integer.parseInt(temp);
        double b = a*1.8 +32;
        return Integer.valueOf(String.valueOf(b));

    }
}
